local S = minetest.get_translator("quikbild")


minetest.register_node("quikbild:climb", {
    description = S("Quikbild Climb-able Node"),
    drawtype = "airlike",
    tiles = {},
    pointable = false,
    buildable_to = true,
    climbable = true,
    walkable = false,
    sunlight_propagates = true,
    paramtype = 'light',
    light_source = 6,
    
})

quikbild.items = {}
local storage = quikbild.storage
local dyes = dye.dyes

for i = 1, #dyes do
    
	local name, desc = unpack(dyes[i])

	minetest.register_node("quikbild:" .. name, {
		description = S("Minigame").." ".. desc,
		tiles = {"wool_" .. name .. ".png"},
        range = 10.0,
		is_ground_content = false,
        groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3,
				flammable = 3, wool = 1},
		sounds = default.node_sound_defaults(),
        on_place = function(itemstack, placer, pointed_thing)
            if placer:is_player() then
                local p_name = placer:get_player_name()
                if arena_lib.is_player_in_arena(p_name, "quikbild") then
                    local arena = arena_lib.get_arena_by_player(p_name)
                    --minetest.chat_send_all('ln17')
                    local pos = pointed_thing.above
                    if pos and minetest.get_node(pos).name == 'air' or string.find(minetest.get_node(pos).name,'quikbild') then  
                        minetest.set_node(pos, {name="quikbild:" .. name})
                        local poss = {}
                        local ser_poss = storage:get_string("pos_"..arena.name)
                        if ser_poss then
                            poss = minetest.deserialize(ser_poss)
                        end
                        table.insert(poss,pos)
                        storage:set_string("pos_"..arena.name,minetest.serialize(poss))
                        return ItemStack("quikbild:" .. name), pos
                    end
                end
            end

        end,
        drop = {},

        on_use = function(itemstack, user, pointed_thing)
            if arena_lib.is_player_in_arena(user:get_player_name(), "quikbild") then
                local pos = pointed_thing.under 
                if pos and string.find(minetest.get_node(pos).name,'quikbild') then  

                    minetest.set_node(pos, {name="quikbild:climb"})
                end
            end
            return nil
        
        end,
		
    })
    table.insert(quikbild.items,"quikbild:" .. name)
end

