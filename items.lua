local S = minetest.get_translator("quikbild")

minetest.register_tool("quikbild:lang", {

    description = S("Choose Language"),
    inventory_image = "quikbild_langchoose.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local p_name = user:get_player_name()
        if p_name then
            quikbild.send_lang_fs(p_name) 
        end
    end

})


minetest.register_tool("quikbild:kick", {

    description = S("Vote to Kick Players"),
    inventory_image = "arenalib_tool_spawner_remove.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local p_name = user:get_player_name()
        local arena = arena_lib.get_arena_by_player(p_name)
        if arena then
            if arena_lib.get_mod_by_player(p_name) == "quikbild" then
                local pl_names = ""
                for pl_name,stats in pairs(arena.players) do
                    pl_names = pl_names .. pl_name .. ","
                end
                local len = string.len(pl_names)
                pl_names = string.sub(pl_names,1,len - 1)
                minetest.show_formspec(p_name, "qb_kick", "formspec_version[5]"..
                "size[8,3.5]"..
                "image_button[0.7,0.6;6.6,0.8;blank.png;;"..S("Kick Players")..";false;true]"..
                "dropdown[0.7,2;3,0.8;kickdropdown;"..pl_names..";1;false]"..
                "button[4.3,2;3,0.8;voteenter;"..S("Vote").."]")
            end
        end

    end

})


minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "qb_kick" then return end
    local p_name = player:get_player_name()
    local arena = arena_lib.get_arena_by_player(p_name)
    if arena then
        if arena_lib.get_mod_by_player(p_name) == "quikbild" then
            if fields.voteenter and fields.kickdropdown then
                local pl_name = fields.kickdropdown
                if arena.players[pl_name] then
                    if not arena.players[pl_name].voters[p_name] then
                        arena.players[pl_name].kickvotes = arena.players[pl_name].kickvotes + 1
                        arena.players[pl_name].voters[p_name] = true
                        minetest.chat_send_player(p_name,minetest.colorize("#7D7071",">> "..S("You voted to kick") .. " " .. pl_name))
                    else
                        minetest.chat_send_player(p_name,minetest.colorize("#7D7071",">> "..S("You have already voted to kick") .. " " .. pl_name.. ". " .. S("You cannot vote twice!")))
                    end
                end
            end
        end
    end
end)

minetest.register_tool("quikbild:help", {

    description = S("Help"),
    inventory_image = "arenalib_editor_info.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local p_name = user:get_player_name()
        if p_name then
            minetest.show_formspec(p_name, "qb_help", "formspec_version[5]".. 
            "size[10.5,5]".. 
            "background9[0,0;0,0;quikbild_gui_bg.png;true;5]".. 
            "textarea[0.8,0.7;8.8,3.5;Helpbox;Quikbild "..S("Help")..";"..S("Each player gets a turn to be Builder. Everyone else guesses what the Builder is building. To guess type your guess in chat. Use lowercase letters only. Answers can be 1 or 2 words. When it is your turn to be Builder you will be shown a word - build it. DO NOT build letters and do not try to tell other players what the word is. That ruins the game for everyone. No one will want to play this with you anymore. If you do not know the word use a search engine to look it up. You have time to do that!").."]")
        end
    end
})


